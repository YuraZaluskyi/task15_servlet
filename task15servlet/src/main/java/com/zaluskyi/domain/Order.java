package com.zaluskyi.domain;

public class Order {
    private static int unique = 1;
    private int id;
    private String titlePizza;
    private int price;
    private boolean status;
    private String shippingAddress;

    public Order(String titlePizza) {
        this.titlePizza = titlePizza;
        id = unique;
        unique++;
    }

    public int getId() {
        return id;
    }

    public String getTitlePizza() {
        return titlePizza;
    }

    public void setTitlePizza(String titlePizza) {
        this.titlePizza = titlePizza;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", titlePizza='" + titlePizza + '\'' +
                '}';
    }
}
