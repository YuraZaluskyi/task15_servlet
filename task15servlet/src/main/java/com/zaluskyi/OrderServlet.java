package com.zaluskyi;

import com.zaluskyi.domain.Order;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/orders/*")
public class OrderServlet extends HttpServlet {

    private static Logger logger1 = LogManager.getLogger(OrderServlet.class);
    private static Map<Integer, Order> orders = new HashMap<>();


    @Override
    public void init() throws ServletException {
        logger1.info("Servlet " + this.getServletName() + " begins");
        Order order1 = new Order("titlePizza1");
        Order order2 = new Order("titlePizza2");
        Order order3 = new Order("titlePizza3");
        Order order4 = new Order("titlePizza4");
        Order order5 = new Order("titlePizza5");
        Order order6 = new Order("titlePizza6");
        Order order7 = new Order("titlePizza7");
        orders.put(order1.getId(), order1);
        orders.put(order2.getId(), order2);
        orders.put(order3.getId(), order3);
        orders.put(order4.getId(), order4);
        orders.put(order5.getId(), order5);
        orders.put(order6.getId(), order6);
        orders.put(order7.getId(), order7);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        logger1.info(this.getServletName() + " into doGet()");
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html><head><body>");
        out.println("<p> <a href='orders'>REFRESH</a> </p>");
        out.println("<p> <a href='groups'>Go to groups</a> </p>");

        out.println("<h2>Orders List</h2>");
        for (Order order : orders.values()) {
            out.println("<p>" + order + "</p>");
        }

        out.println("<form action='orders' method='POST'>\n"
                + "  Pizza title: <input type='text' name='pizza_title'>\n"
                + "  <button type='submit'>Save Order</button>\n"
                + "</form>");

        out.println("<form>\n"
                + "  <p><b>DELETE ELEMENT</b></p>\n"
                + "  <p> Order id: <input type='text' name='order_id'>\n"
                + "    <input type='button' onclick='remove(this.form.order_id.value)' name='ok' value='Delete Order'/>\n"
                + "  </p>\n"
                + "</form>");

        out.println("<script type='text/javascript'>\n"
                + "  function remove(id) { fetch('orders/' + id, {method: 'DELETE'}); }\n"
                + "</script>");

        // Echo client's request information
        out.println("<p>Request URI: " + req.getRequestURI() + "</p>");
        out.println("<p>Method: " + req.getMethod() + "</p>");
        out.println("</body></html>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        logger1.info("----------doPost------------");
        String newPizzaTitle = req.getParameter("pizza_title");
        logger1.info("Add " + newPizzaTitle);
        Order newOrder = new Order(newPizzaTitle);
        orders.put(newOrder.getId(), newOrder);
        doGet(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        logger1.info("----------doDelete------------");
        String id = req.getRequestURI();
        logger1.info("URI=" + id);
        id = id.replace("/zaluskyi-servlets/orders/", "");
        logger1.info("id=" + id);
        orders.remove(Integer.parseInt(id));
    }

    @Override
    public void destroy() {
        logger1.info("Servlet " + this.getServletName() + " has stopped");
    }

//    @Override
//    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        logger.info(this.getServletName() + "info doGet()");
//        resp.setContentType("text/html");
//        PrintWriter out = resp.getWriter();
//        out.println("<html><head><body>");
//        out.println("<p> <p> <a href='orders'>REFRESH</a> </p>");
//
//        out.println("<h2>Orders List</h2>");
//        for (Order order : orders.values()) {
//            out.println("<p>" + order + "</p>");
//        }
//
//        out.println("<form action='orders' method='POST'>\n"
//                + "  Order title pizza: <input type='text' name='pizza_title'>\n"
//                + " Order price pizza: <input type='number' name='price_pizza'\n"
//                + "Order address shipping: <input type='text' name='address_shipping\n"
//                + "  <button type='submit'>Save Order</button>\n"
//                + "</form>");
//
//        out.println("</body></html>");
//    }
}
